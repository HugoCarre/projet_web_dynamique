from models import Flux
import feedparser

def getFluxOfUser(user):
    """
    Obtient la liste des flux liés à l'utilisateur
    """
    return Flux.select().where(Flux.user == user.id)

def getParsedFlux(user):
    """
    Traduit les flux en objets et retourne la liste de ces objets
    """
    listParsedFlux = []
    flux = getFluxOfUser(user)
    for flu in flux:
        listParsedFlux.append(parseFlux(flu))

    return listParsedFlux

def parseFlux(flux):
    """
    Traduit un flux
    """
    return feedparser.parse(flux.url)