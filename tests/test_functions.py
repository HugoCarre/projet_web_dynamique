import pytest
from libraries.tools import getFluxOfUser, getParsedFlux, parseFlux
from models import Flux,User

def test_getFluxOfUser():
    assert len(getFluxOfUser(User.get(name="test"))) == len(Flux.select().where(Flux.user == User.get(name="test").id))

def test_getParsedFlux():
    assert len(getParsedFlux(User.get(name="test"))) == len(Flux.select().where(Flux.user == User.get(name="test").id))