import os
import pytest
from app import app
from flask import url_for
from models import User
from flask_login import login_user

@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'TEST'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client

def test_index(client):
    rv = client.get('/')
    assert rv.status_code == 200
    assert b'<a class="button" href="/login">Se connecter</a>' in rv.data

    assert b'<a class="button" href="/register">' in rv.data

def test_login(client):
    rv = client.get('/login')
    assert rv.status_code == 200

def test_register(client):
    rv = client.get('/register')
    assert rv.status_code == 200

def test_account(client):
    rv = client.get('/account')
    assert rv.status_code == 401 #code de retour si non connecté


def test_addFlux(client):
    rv = client.get('/addflux')
    assert rv.status_code == 401

def test_showFlux(client):
    rv = client.get('/showflux/1',)
    assert rv.status_code == 401