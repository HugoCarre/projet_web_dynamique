# Projet_web_dynamique

Hugo Carré LPRGI-Init
Kévin Fidelin LPRGI-alternant Cisco

utilisateur de test:
    email: test@test.fr
    mot de passe: test




Partie templates : 

La page base.html définit la tête du document html ainsi que le header et le footer du corps du document.
Les autres pages font appels à Jinja avec un include pour reprendre base.html.

La première page est la page d'accueil.
Sur celle-ci, un utilsateur peut : 
	- se connecter à son compte pour voir ses flux
	- se créer un compte 

Une fois connecté, l'utilisateur peut voir ses flux.
Il peut en ajouter, en supprimer ou les consulter.

S'il en ajoute, il est redirigé sur la page adéquate.
Pour ajouter un flux, il faut entrer un nom ainsi que l'url du flux.

S'il décide de supprimer un flux, la page s'actualise et le flux supprimé n'apparaît plus

Enfin, s'il décide de consulter un flux, il est redirigé vers une page qui affiche le contenu du flux séléctionné, c'est-à-dire, les différents articles.

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Dans la partie Python :

On définit les routes correspondant à chaque template avec flask dans app.py. Pour les routes account, addflux, deleteflux, showflux, et logout, il faut que l'utilisateur se soit connecté.

Dans models.py, on définit les deux tables de la base de données sqlite3 que sont User et Flux, ainsi que la création de 2 fonctions pour créer les tables ou les supprimer.

Dans forms.py, on définit les formulaires qui vont être utiliser dans les templates pour ajouter un flux, inscrire un utilisateur ou se connecter.

Nous avons créé un dossier "libraries" dans lequel il y a le fichier tools.py qui définit 3 fonctions permettant la gestion des flux afin d'obtenir les flux d'un utilisateur et les afficher.

Dans le dossier "tests", il y a les tests qui montrent que les fonctions renvoient bien les résultats attendus, et les tests montrant que les pages fonctionnent.
Ces tests sont réalisés à l'aide de pytest.