from peewee import *
from flask_login import UserMixin
import datetime

database = SqliteDatabase("users.sqlite3")

class BaseModel(Model):
    class Meta:
        database = database

class User(BaseModel, UserMixin):
    """
    Classe décrivant un utilisateur
    """
    name = CharField()
    email = CharField(unique=True)
    password = CharField()

class Flux(BaseModel):
    """
    Classe décrivant un flux appartenant à un utilisateur
    """
    user = ForeignKeyField(User,backref="flux")
    nom = CharField()
    url = CharField()
    

def create_tables():
    """
    Permet de créer les tables dans la base de données
    """
    with database:
        database.create_tables([User,Flux,])

def drop_tables():
    """
    Permet d'effacer les tables de la base de données
    """
    with database:
        database.drop_tables([User,Flux,])