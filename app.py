import click
from flask import *
from models import *
from forms import *
from flask_login import LoginManager, login_user, login_required, current_user,logout_user
import bcrypt
from libraries.tools import getParsedFlux, parseFlux, getFluxOfUser

app = Flask(__name__)
app.secret_key = "hr9gu0tyTR46FUfu4iGFuyFif1M%pK/bj"
login_manager = LoginManager()
login_manager.init_app(app)

@app.cli.command()
def initdb():
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropbd():
    drop_tables()
    click.echo('Dropped tables from database')

@app.route('/')
def index():
  """
    Route vers la page d'accueil
  """
  if(current_user.is_authenticated ):
    return redirect(url_for('account'))

  return render_template('index.html')

@app.route('/account')
@login_required
def account():
  """
  Route vers la page montrant les flux.
  Utilisateur connecté obligatoire.
  """
  return render_template('account.html',flux=getParsedFlux(current_user),fluxEntities=getFluxOfUser(current_user))


@app.route('/addflux', methods=['GET','POST'])
@login_required
def addFlux():
  """
    Pour ajouter un flux avec un form auquel on donne le nom du site et l'url du site.
    L'utilisateur lié au flux est directement rempli grâce à current_user.
    Un utilisateur connecté est obligatoire.
  """
  flux = Flux()
  form = FluxForm()
  if form.validate_on_submit():
    form.populate_obj(flux)
    flux.user = current_user.id
    flux.save()
    return redirect(url_for('account'))
    
  return render_template('addFlux.html',form=form)

@app.route('/deleteflux/<int:flux_id>', methods=['GET','POST'])
@login_required
def deleteFlux(flux_id):
  """
    Pour supprimer le flux choisi.
    Un utilisateur connecté est obligatoire.
  """
  try:
    flux = Flux.get(id=flux_id)
  except DoesNotExist:
    return redirect(url_for('account'))

  flux.delete_instance()
    
  return redirect(url_for('account'))

@app.route('/showflux/<int:flux_id>', methods=['GET','POST'])
@login_required
def showFlux(flux_id):
  """
    Pour afficher le flux choisi.
    Un utilisateur connecté est obligatoire.
  """
  try:
    flux= Flux.get(id=flux_id)
  except DoesNotExist:
    return redirect(url_for('account'))
  
    
  return render_template('showFlux.html',flux=parseFlux(flux))

@app.route('/login', methods=['GET','POST'])
def login():
  """
    Pour se connecter, si un utilisateur entre l'email et le bon mot de passe,
    on connecte l'utilisateur concerné.
  """
  if(current_user.is_authenticated ):
    return redirect(url_for('account'))
  form = LoginForm()
  if form.validate_on_submit():
    try:
      user = User.get(email=form.email.data)
    except DoesNotExist:
      user = None
    
    if user :
      if bcrypt.checkpw(form.password.data.encode('utf-8'),user.password.encode('utf-8')):
        login_user(user)
        return redirect(url_for('account'))
      else:
        flash("Mauvais identifiants")
        return redirect(url_for('login'))
    else:
      flash("Mauvais identifiants")
      return redirect(url_for('login'))

  return render_template("login.html", form=form)

@app.route("/logout")
@login_required
def logout():
  """
    Pour se déconnecter
  """
  logout_user()
  return redirect(url_for('index'))

@app.route('/register', methods=['GET','POST'])
def register():
  """
    Pour enregistrer un utilisateur, demande le nom, l'email et un mot de passe.
    L'email est unique, deux utilisateurs avec la même email ne peuvent exister.
  """
  if(current_user.is_authenticated ):
    return redirect(url_for('account'))
  user = User()
  form = RegisterForm()
  if form.validate_on_submit():
    try:
      exist = User.get(email=form.email.data)
    except DoesNotExist:
      exist = None

    if exist:
      flash("Email déjà utilisée !")
      return redirect(url_for('register'))
    form.populate_obj(user)
    user.password = bcrypt.hashpw(form.password.data.encode('utf-8'),bcrypt.gensalt())
    user.save()
    return redirect(url_for('index'))

  return render_template("register.html", form=form)

@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)
