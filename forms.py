from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, PasswordField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length


class LoginForm(FlaskForm):
    """
    Formulaire pour pouvoir se connecter
    """
    email = StringField('Email', validators= [DataRequired(),])
    password = PasswordField('Mot de passe',validators= [DataRequired(),])

class FluxForm(FlaskForm):
    """
    Formulaire pour pouvoir ajouter un flux
    """
    nom = StringField('Nom du flux', validators=[DataRequired(),])
    url = StringField('Url du flux', validators=[DataRequired(),])

class RegisterForm(FlaskForm):
    """
    Formulaire pour pouvoir s'inscrire sur le site
    """
    name = StringField('Nom', validators=[DataRequired(),])
    email = StringField('Email', validators= [DataRequired(),])
    password = PasswordField('Mot de passe', validators=[DataRequired(),])